Optimization
============

Contents:

.. toctree::
   :maxdepth: 2

   optimization/basics
   optimization/time_discretization
   optimization/modelica_models
   optimization/csv_io
   optimization/fews_io
   optimization/lookup_tables
   optimization/multi_objective
   optimization/forecast_uncertainty
