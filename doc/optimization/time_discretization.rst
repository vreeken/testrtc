Time discretization
===================

.. autoclass:: rtctools.optimization.collocated_integrated_optimization_problem.CollocatedIntegratedOptimizationProblem
    :members: theta, integrated_states, integrator_options
    :show-inheritance: