Simulation
==========

.. autoclass:: rtctools.simulation.simulation_problem.SimulationProblem
    :members:
    :show-inheritance:

.. autofunction:: rtctools.util.run_simulation_problem